package com.nayax.secondmicrocervice.dto;

public class ShortUserDto {
    Long id;
    String fullName;

    public ShortUserDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
