package com.nayax.secondmicrocervice.dto;

public class CompanyHierarchyDto {
    Long id;
    String hierdId;
    Boolean hasRight;

    public CompanyHierarchyDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHierdId() {
        return hierdId;
    }

    public void setHierdId(String hierdId) {
        this.hierdId = hierdId;
    }

    public Boolean getHasRight() {
        return hasRight;
    }

    public void setHasRight(Boolean hasRight) {
        this.hasRight = hasRight;
    }

    @Override
    public String toString() {
        return "CompanyHierarchyDto{" +
                "id=" + id +
                ", hierdId='" + hierdId + '\'' +
                ", hasRight=" + hasRight +
                '}';
    }
}
