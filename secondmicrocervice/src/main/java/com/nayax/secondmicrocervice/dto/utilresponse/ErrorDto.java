package com.nayax.secondmicrocervice.dto.utilresponse;
import lombok.Data;

@Data
public class ErrorDto {
    private String errorDto;

    public ErrorDto(String errorDto) {
        this.errorDto = errorDto;
    }
}
