package com.nayax.secondmicrocervice.dto;

public class CompanyDto {
    Long id;
    Long distribId;
    String type;
    String name;
    String nodeText;
    Boolean hasRight;

    public CompanyDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDistribId() {
        return distribId;
    }

    public void setDistribId(Long distribId) {
        this.distribId = distribId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNodeText() {
        return nodeText;
    }

    public void setNodeText(String nodeText) {
        this.nodeText = nodeText;
    }

    public Boolean getHasRight() {
        return hasRight;
    }

    public void setHasRight(Boolean hasRight) {
        this.hasRight = hasRight;
    }

    @Override
    public String toString() {
        return "CompanyDto{" +
                "id=" + id +
                ", distribId=" + distribId +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", nodeText='" + nodeText + '\'' +
                ", hasRight=" + hasRight +
                '}';
    }
}
