package com.nayax.secondmicrocervice.dto.utilresponse;

import java.util.List;
import lombok.Data;

@Data
public class ResponseDto<T> {
    private T data;
    private List<ErrorDto> errors;
}
