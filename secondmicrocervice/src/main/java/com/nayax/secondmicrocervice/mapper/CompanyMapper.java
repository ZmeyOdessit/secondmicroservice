package com.nayax.secondmicrocervice.mapper;

import com.nayax.secondmicrocervice.dto.CompanyDto;
import com.nayax.secondmicrocervice.entity.CompanyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CompanyRightConvert.class)
public interface CompanyMapper {
    CompanyMapper INSTANCE = Mappers.getMapper(CompanyMapper.class);

    @Mapping(target = "hasRight", source = "companyDto.hasRight")
    CompanyEntity companyDtoToCompanyEntity(CompanyDto companyDto);

    @Mapping(target = "hasRight", source = "companyEntity.hasRight")
    CompanyDto companyEntityToCompanyDto (CompanyEntity companyEntity);

}
