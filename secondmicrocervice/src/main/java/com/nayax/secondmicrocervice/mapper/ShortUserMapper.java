package com.nayax.secondmicrocervice.mapper;

import com.nayax.secondmicrocervice.dto.ShortUserDto;
import com.nayax.secondmicrocervice.entity.ShortUserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ShortUserMapper {

    ShortUserMapper INSTANCE = Mappers.getMapper(ShortUserMapper.class);

    ShortUserEntity userDtoToUserEntity(ShortUserDto userDto);

    ShortUserDto userEntityToUserDto (ShortUserEntity userEntity);
}
