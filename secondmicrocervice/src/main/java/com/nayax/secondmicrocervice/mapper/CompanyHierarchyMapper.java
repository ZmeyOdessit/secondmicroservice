package com.nayax.secondmicrocervice.mapper;

import com.nayax.secondmicrocervice.dto.CompanyDto;
import com.nayax.secondmicrocervice.dto.CompanyHierarchyDto;
import com.nayax.secondmicrocervice.entity.CompanyHierarchyEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CompanyHierarchyRightConvert.class)
public interface CompanyHierarchyMapper {
    CompanyHierarchyMapper INSTANCE = Mappers.getMapper(CompanyHierarchyMapper.class);

   @Mapping(target = "hasRight", source = "companyHierarchyDto.hasRight")
   CompanyHierarchyEntity companyHrDtoToCompanyEntity(CompanyHierarchyDto companyHierarchyDto);

    @Mapping(target = "hasRight", source = "companyHierarchyEntity.hasRight")
    CompanyDto companyHrEntityToCompanyDto (CompanyHierarchyEntity companyHierarchyEntity);
}
