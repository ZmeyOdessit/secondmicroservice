package com.nayax.secondmicrocervice.mapper;

public class CompanyHierarchyRightConvert {
    public boolean stringToBoolean(String str) {
        return str.equals("Y");
    }

    public String boolToString(Boolean bool) {
        return bool ? "Y" : "N";
    }
}
