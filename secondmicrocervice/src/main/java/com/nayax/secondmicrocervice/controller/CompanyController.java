package com.nayax.secondmicrocervice.controller;

import com.nayax.secondmicrocervice.controller.feign.Feign;
import com.nayax.secondmicrocervice.dto.CompanyDto;
import com.nayax.secondmicrocervice.dto.ShortCompanyDto;
import com.nayax.secondmicrocervice.dto.ShortUserDto;
import com.nayax.secondmicrocervice.dto.utilresponse.ResponseDto;
import com.nayax.secondmicrocervice.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @Autowired
    Feign feign;

    @GetMapping("/company/findAll")
    public ResponseEntity<Object> getAllCompany() {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(companyService.findAll());
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping("/company/find")
    public ResponseEntity<Object> findById(@RequestParam Long id) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(companyService.findById(id));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @PostMapping("/company/create")
    public ResponseEntity<Object> add (@RequestBody CompanyDto companyDto, @RequestParam Long id) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        companyDto = companyService.create(companyDto, id);
        Boolean isAdded = feign.addCompanyShort(new ShortCompanyDto(companyDto.getId(), companyDto.getName())).getBody().getData();

        if (isAdded == null || !isAdded){
            companyService.deleteById(companyDto.getId());
            throw new IllegalArgumentException("First service doesn't work");
        }
        responseDto.setData(companyDto);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @PostMapping("/company/update")
    public ResponseEntity<Object> update (@RequestBody CompanyDto companyDto, @RequestParam Long id) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(companyService.update(companyDto, id));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @DeleteMapping("/company/delete")
    public ResponseEntity<Object> deleteById(@RequestParam Long id) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(companyService.deleteById(id));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }



}
