package com.nayax.secondmicrocervice.controller;

import com.nayax.secondmicrocervice.dto.ShortUserDto;
import com.nayax.secondmicrocervice.dto.utilresponse.ResponseDto;
import com.nayax.secondmicrocervice.service.ShortUserService;
import com.nayax.secondmicrocervice.service.UserShortCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    @Autowired
    ShortUserService shortUserService;

    @Autowired
    UserShortCompanyService userShortCompanyService;

    @PostMapping("/user/create")
    public ResponseEntity<Object> add(@RequestBody ShortUserDto user, @RequestParam Long idCompany) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(shortUserService.create(user, idCompany));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping("/user/find")
    public ResponseEntity<Object> findById (@RequestParam Long id) {

        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(shortUserService.findById(id));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @GetMapping("/user/findAll")
    public ResponseEntity<Object> findAll() {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(shortUserService.findAll());
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @DeleteMapping("/user/delete")
    public ResponseEntity<Object> deleteById(@RequestParam Long id) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(shortUserService.deleteById(id));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @PutMapping("/user/update")
    public ResponseEntity<Object> update(@RequestBody ShortUserDto userDto) {
        ResponseDto<Object> responseDto = new ResponseDto<>();
        responseDto.setData(shortUserService.update(userDto));
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
