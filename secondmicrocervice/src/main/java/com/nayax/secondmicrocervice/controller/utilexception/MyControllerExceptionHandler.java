package com.nayax.secondmicrocervice.controller.utilexception;

import com.nayax.secondmicrocervice.dto.utilresponse.ErrorDto;
import com.nayax.secondmicrocervice.dto.utilresponse.ResponseDto;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;

import static org.springframework.http.HttpStatus.OK;

@RestControllerAdvice
public class MyControllerExceptionHandler {

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        ResponseDto<Void> responseDto = new ResponseDto<>();
        ErrorDto errorDto = new ErrorDto(ex.getMessage());
        responseDto.setErrors(Collections.singletonList(errorDto));
        return new ResponseEntity<>(responseDto, OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
        ResponseDto<Void> responseDto = new ResponseDto<>();
        ErrorDto errorDto = new ErrorDto(ex.getMessage());
        responseDto.setErrors(Collections.singletonList(errorDto));
        return new ResponseEntity<>(responseDto, OK);
    }

}
