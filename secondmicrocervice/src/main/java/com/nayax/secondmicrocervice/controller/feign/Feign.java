package com.nayax.secondmicrocervice.controller.feign;


import com.nayax.secondmicrocervice.dto.ShortCompanyDto;
import com.nayax.secondmicrocervice.dto.utilresponse.ResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(value = "feign", url = "http://localhost:8097")
public interface Feign {

    @RequestMapping(method = RequestMethod.POST, value = "/company/add", produces = "application/json")
    ResponseEntity<ResponseDto<Boolean>> addCompanyShort(@RequestBody ShortCompanyDto shortCompanyDto);

}
