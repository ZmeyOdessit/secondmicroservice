package com.nayax.secondmicrocervice.entity;

public class ShortUserEntity {
    Long id;
    String fullName;

    public ShortUserEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
