package com.nayax.secondmicrocervice.entity;

public class CompanyHierarchyEntity {
    Long id;
    String hierdId;
    String hasRight;

    public CompanyHierarchyEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHierdId() {
        return hierdId;
    }

    public void setHierdId(String hierdId) {
        this.hierdId = hierdId;
    }

    public String getHasRight() {
        return hasRight;
    }

    public void setHasRight(String hasRight) {
        this.hasRight = hasRight;
    }

    @Override
    public String toString() {
        return "CompanyHierarchyEntity{" +
                "id=" + id +
                ", hierdId='" + hierdId + '\'' +
                ", hasRight='" + hasRight + '\'' +
                '}';
    }
}
