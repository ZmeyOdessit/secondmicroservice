package com.nayax.secondmicrocervice.repository;

import com.nayax.secondmicrocervice.entity.ShortUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class ShortUserRepository {

    @Autowired
    JdbcTemplate template;

    private final RowMapper<ShortUserEntity> userRowMapper = (rs, rowNum) -> {
        ShortUserEntity userEntity = new ShortUserEntity();
        userEntity.setId(rs.getLong("Id"));
        userEntity.setFullName(rs.getString("FullName"));
        return userEntity;
    };

    public ShortUserEntity createUser(ShortUserEntity user) {
        if (user == null) {
            throw new IllegalArgumentException("Passed an empty object!");
        }
        String sql = "INSERT INTO [UserShort] (Id, FullName) VALUES (?, ?);";
        template.update(sql,user.getId(),user.getFullName());
        return findById(user.getId());
    }

    public ShortUserEntity findById(Long id) {
        String sql = "select id, FullName\n" +
                "from [UserShort]\n" +
                "where Id = ?";
        try {
            return template.queryForObject(sql, userRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<ShortUserEntity> findAll() {
        String sql = "select id, FullName\n" +
                "from [UserShort]";
        return template.query(sql, userRowMapper);
    }

    public ShortUserEntity deleteById(Long id) {
        ShortUserEntity deletedUser = findById(id);
        String sql = "Delete [UserShort] Where Id = ?";
        template.update(sql, id);
        return deletedUser;
    }

    public ShortUserEntity update(ShortUserEntity userEntity) {
        String sql = "UPDATE [UserShort] \n" +
                "SET FullName = ?\n" +
                "WHERE Id = ?;";
       template.update(sql, userEntity.getFullName(), userEntity.getId());
        return findById(userEntity.getId());
    }


}
