package com.nayax.secondmicrocervice.repository;

import com.nayax.secondmicrocervice.entity.CompanyHierarchyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;

@Repository
public class CompanyHierarchyRepository {

    @Autowired
    JdbcTemplate template;

    private final RowMapper<CompanyHierarchyEntity> companyEntityRowMapper = (rs, rowNum) -> {
        CompanyHierarchyEntity companyEntity = new CompanyHierarchyEntity();
        companyEntity.setId(rs.getLong("Id"));
        companyEntity.setHierdId(rs.getString("HierdId"));
        companyEntity.setHasRight(rs.getString("HasRight"));
        return companyEntity;
    };

    public String getNodeText(Long id) {
        String sql = " DECLARE @NodeText HIERARCHYID = \n" +
                "(select ch.HierdId\n" +
                " from [Company] c\n" +
                " join [CompanyHierarchy] ch on c.DistrbId=ch.Id\n" +
                " where c.id = ?);\n" +
                "\n" +
                "select @NodeText.ToString () AS [NodeText]";

        return template.queryForObject(sql, String.class, id);
    }

    public Integer getNumbersOfHeir(Long id) {
        String sql = " DECLARE @Node HIERARCHYID = \n" +
                "(select ch.HierdId\n" +
                " from [Company] c\n" +
                " join [CompanyHierarchy] ch on c.DistrbId=ch.Id\n" +
                " where c.id = ?);\n" +
                "\n" +
                "DECLARE @Level SMALLINT = @Node.GetLevel()\n" +
                "\n" +
                "\n" +
                " select c. Id, c.DistrbId, c.Name, c.Type, ch.HierdId.ToString () AS [HierdId], ch.HierdId.GetLevel() as [Node Level], ch.HasRight\n" +
                " from [Company] c\n" +
                " join [CompanyHierarchy] ch on c.DistrbId=ch.Id\n" +
                " where ch.HierdId.IsDescendantOf(@Node)=1\n" +
                " AND ch.HierdId.GetLevel() = @Level + 1";

        return template.query(sql, companyEntityRowMapper, id).size();
    }

    public CompanyHierarchyEntity createHierarchy(CompanyHierarchyEntity companyHierarchy) {
        if (companyHierarchy == null) {
            throw new IllegalArgumentException("Passed an empty object!");
        }
        String sql = " Insert into [CompanyHierarchy] (HierdId, HasRight)\n" +
                " values (?, ?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql,
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, companyHierarchy.getHierdId());
            ps.setString(2, companyHierarchy.getHasRight());
            return ps;
        }, keyHolder);
        if (keyHolder.getKey() == null) {
            throw new IllegalArgumentException();
        } else {
            return findById(keyHolder.getKey().longValue());
        }

    }

    public CompanyHierarchyEntity findById(Long id) {
        String sql = "select *\n" +
                "from [CompanyHierarchy]\n" +
                "where id = ?;";
        try {
            return template.queryForObject(sql, companyEntityRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public void deleteById(Long id) {
        //CompanyHierarchyEntity deletedHierarchy = findById(id);
        String sql = "Delete [CompanyHierarchy] \n" +
                "Where Id = ?";
        template.update(sql, id);
        //return deletedHierarchy;
    }
}
