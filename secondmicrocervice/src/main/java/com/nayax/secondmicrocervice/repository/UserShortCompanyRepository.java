package com.nayax.secondmicrocervice.repository;

import com.nayax.secondmicrocervice.entity.ShortUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserShortCompanyRepository {
    @Autowired
    JdbcTemplate template;

    public boolean createUserShortCompany(Long idUser, Long idCompany) {
        if (idUser == null || idCompany == null) {
            throw new IllegalArgumentException("Passed an empty object!");
        }
        String sql = " Insert into [UserShortCompany] (UserSortId, CompanyId) \n" +
                " values (?, ?);";
        template.update(sql,idUser,idCompany);
        return true;
    }

}
