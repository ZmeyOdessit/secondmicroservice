package com.nayax.secondmicrocervice.repository;

import com.nayax.secondmicrocervice.entity.CompanyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    JdbcTemplate template;

    private final RowMapper<CompanyEntity> companyEntityRowMapperRowMapper = (rs, rowNum) -> {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setId(rs.getLong("Id"));
        companyEntity.setDistribId(rs.getLong("DistrbId"));
        companyEntity.setType(rs.getString("Type"));
        companyEntity.setName(rs.getString("Name"));
        companyEntity.setNodeText(rs.getString("NodeText"));
        companyEntity.setHasRight(rs.getString("HasRight"));
        return companyEntity;
    };


    public List<CompanyEntity> findAll() {
        String sql = " select c. Id, c.DistrbId, c.Name, c.Type, ch.HierdId.ToString () AS [NodeText], ch.HasRight\n" +
                " from [Company] c\n" +
                " join [CompanyHierarchy] ch on c.DistrbId=ch.Id";
        return template.query(sql, companyEntityRowMapperRowMapper);
    }

    public CompanyEntity findById(Long id) {
        String sql = "  select c. Id, c.DistrbId, c.Name, c.Type, ch.HierdId.ToString () AS [NodeText], ch.HasRight\n" +
                " from [Company] c\n" +
                " join [CompanyHierarchy] ch on c.DistrbId=ch.Id\n" +
                " where c.id = ?";

        try {
            return template.queryForObject(sql, companyEntityRowMapperRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public CompanyEntity createCompany(CompanyEntity company) {
        if (company == null) {
            throw new IllegalArgumentException("Passed an empty object!");
        }
        String sql = "Insert into [Company] (DistrbId, Type, Name) \n" +
                " values (?, ?, ?);";

        KeyHolder keyHolder = new GeneratedKeyHolder();
        System.out.println(company);
        template.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql,
                    Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, company.getDistribId());
            ps.setString(2, company.getType());
            ps.setString(3, company.getName());
            return ps;
        }, keyHolder);
        if (keyHolder.getKey() == null) {
            throw new IllegalArgumentException();
        } else {
            return findById(keyHolder.getKey().longValue());
        }
    }

    public CompanyEntity deleteById(Long id) {
        CompanyEntity deletedCompany = findById(id);
        String sql = "Delete [Company] \n" +
                "Where Id = ?";
        template.update(sql, id);
        return deletedCompany;
    }

}
