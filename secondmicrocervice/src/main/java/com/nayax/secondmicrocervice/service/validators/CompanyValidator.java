package com.nayax.secondmicrocervice.service.validators;


import com.nayax.secondmicrocervice.dto.CompanyDto;
import com.nayax.secondmicrocervice.entity.CompanyEntity;
import org.springframework.stereotype.Component;

@Component
public class CompanyValidator {

    public boolean validateCompany(CompanyDto companyDto){
        if (companyDto == null){
            throw new IllegalArgumentException("Company is null");
        } else if (companyDto.getName() == null){
            throw new IllegalArgumentException("Invalid name");
        } else if (companyDto.getType() == null){
            throw new IllegalArgumentException("Invalid type of company");
        } else if (companyDto.getHasRight() == null){
            throw new IllegalArgumentException("Invalid right of company");
        }
        return true;
    }

   public boolean isIdCorrect (Long id) {
       if (id == null || id <= 0){
           throw new IllegalArgumentException("Invalid Id");
       }
       return true;
    }

    public boolean isCompanyExist (CompanyEntity companyEntity) {
       if (companyEntity == null ) {
           throw new IllegalArgumentException("Company don't exist in table!!!");
       }
       return true;
    }
}
