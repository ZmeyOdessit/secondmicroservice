package com.nayax.secondmicrocervice.service;

import com.nayax.secondmicrocervice.dto.ShortUserDto;
import com.nayax.secondmicrocervice.entity.ShortUserEntity;
import com.nayax.secondmicrocervice.mapper.ShortUserMapper;
import com.nayax.secondmicrocervice.repository.ShortUserRepository;
import com.nayax.secondmicrocervice.service.validators.ShortUserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class ShortUserService {

    @Autowired
    ShortUserRepository shortUserRepository;

    @Autowired
    ShortUserValidator shortUserValidator;

    @Autowired
    UserShortCompanyService userShortCompanyService;

    public ShortUserDto create(ShortUserDto user, Long idCompany) {
        if (shortUserValidator.validIdCompany(idCompany)) {
            if (shortUserValidator.validateUser(user)) {
                if (!shortUserValidator.isUserExist(user)) {
                    ShortUserEntity shortUserEntity = shortUserRepository.createUser(ShortUserMapper.INSTANCE.userDtoToUserEntity(user));
                    userShortCompanyService.create(shortUserEntity.getId(), idCompany);
                    return ShortUserMapper.INSTANCE.userEntityToUserDto(shortUserEntity);
                } else {
                    userShortCompanyService.create(user.getId(), idCompany);
                    return ShortUserMapper.INSTANCE.userEntityToUserDto(ShortUserMapper.INSTANCE.userDtoToUserEntity(user));
                }

            }
        }
        return null;
    }

    public ShortUserDto findById(Long id) {
        ShortUserEntity userEntity = null;
        if (shortUserValidator.isIdCorrect(id)) {
            userEntity = shortUserRepository.findById(id);
        }
        return ShortUserMapper.INSTANCE.userEntityToUserDto(userEntity);
    }

    public List<ShortUserDto> findAll() {
        List<ShortUserDto> list = new ArrayList<>();
        for (ShortUserEntity entity : shortUserRepository.findAll()) {
            list.add(ShortUserMapper.INSTANCE.userEntityToUserDto(entity));
        }
        return list;
    }

    public ShortUserDto deleteById(Long id) {
        ShortUserEntity userEntity = null;
        if (shortUserValidator.isIdCorrect(id)) {
            userEntity = shortUserRepository.deleteById(id);
        }
        return ShortUserMapper.INSTANCE.userEntityToUserDto(userEntity);
    }

    public ShortUserDto update(ShortUserDto shortUserDto) {
        return ShortUserMapper.INSTANCE.userEntityToUserDto(shortUserRepository.
                update(ShortUserMapper.INSTANCE.userDtoToUserEntity(shortUserDto)));

    }

}
