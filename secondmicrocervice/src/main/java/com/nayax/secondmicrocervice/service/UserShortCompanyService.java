package com.nayax.secondmicrocervice.service;

import com.nayax.secondmicrocervice.dto.ShortUserDto;
import com.nayax.secondmicrocervice.entity.ShortUserEntity;
import com.nayax.secondmicrocervice.mapper.ShortUserMapper;
import com.nayax.secondmicrocervice.repository.UserShortCompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserShortCompanyService {

    @Autowired
    UserShortCompanyRepository userShortCompanyRepository;

    public Boolean create(Long idUser, Long idCompany) {
        userShortCompanyRepository.createUserShortCompany(idUser,idCompany);
        return true;
    }
}
