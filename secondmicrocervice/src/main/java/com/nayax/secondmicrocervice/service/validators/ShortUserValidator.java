package com.nayax.secondmicrocervice.service.validators;


import com.nayax.secondmicrocervice.dto.ShortUserDto;
import com.nayax.secondmicrocervice.repository.CompanyRepository;
import com.nayax.secondmicrocervice.repository.ShortUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShortUserValidator {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    ShortUserRepository shortUserRepository;

    public boolean validateUser (ShortUserDto userDto){
        if (userDto == null){
            throw new IllegalArgumentException("User is null");
        } else if (userDto.getId() == null){
            throw new IllegalArgumentException("Invalid Id");
        } else if (userDto.getFullName() == null){
            throw new IllegalArgumentException("Invalid name of User");
        }
        return true;
    }

    public boolean validIdCompany (Long idCompany) {
        if (companyRepository.findById(idCompany)==null) {
            throw new IllegalArgumentException("Invalid Id Company, Company don't exist!");
        }
        return true;
    }
    public boolean isUserExist (ShortUserDto userDto) {
        return shortUserRepository.findById(userDto.getId()) != null;
    }

    public boolean isIdCorrect (Long id) {
        if (id == null || id <= 0){
            throw new IllegalArgumentException("Invalid Id");
        }
        return true;
    }
}
