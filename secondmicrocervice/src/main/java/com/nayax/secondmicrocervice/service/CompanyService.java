package com.nayax.secondmicrocervice.service;

import com.nayax.secondmicrocervice.dto.CompanyDto;

import com.nayax.secondmicrocervice.dto.CompanyHierarchyDto;
import com.nayax.secondmicrocervice.entity.CompanyEntity;
import com.nayax.secondmicrocervice.entity.CompanyHierarchyEntity;
import com.nayax.secondmicrocervice.mapper.CompanyHierarchyMapper;
import com.nayax.secondmicrocervice.mapper.CompanyMapper;
import com.nayax.secondmicrocervice.repository.CompanyHierarchyRepository;
import com.nayax.secondmicrocervice.repository.CompanyRepository;
import com.nayax.secondmicrocervice.service.validators.CompanyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    CompanyHierarchyRepository companyHierarchyRepository;

    @Autowired
    CompanyValidator companyValidator;

    public List<CompanyDto> findAll() {
        List<CompanyDto> companyEntityList = new ArrayList<>();
        for (CompanyEntity entity : companyRepository.findAll()) {
            companyEntityList.add(CompanyMapper.INSTANCE.companyEntityToCompanyDto(entity));
        }
        return companyEntityList;
    }

    public CompanyDto findById(Long id) {
        CompanyEntity companyEntity = companyRepository.findById(id);
        if (companyValidator.isCompanyExist(companyEntity)) {
            return CompanyMapper.INSTANCE.companyEntityToCompanyDto(companyEntity);
        }
        return null;
    }

    public CompanyDto create(CompanyDto companyDto, Long id) {
        if (companyValidator.validateCompany(companyDto)) {
            CompanyHierarchyDto companyHierarchyDto = new CompanyHierarchyDto();
            companyHierarchyDto.setHasRight(companyDto.getHasRight());
            companyHierarchyDto.setHierdId(companyHierarchyRepository.getNodeText(id) +
                    (companyHierarchyRepository.getNumbersOfHeir(id) + 1) + "/");
            CompanyHierarchyEntity companyHierarchyEntity = companyHierarchyRepository.createHierarchy(CompanyHierarchyMapper.
                    INSTANCE.companyHrDtoToCompanyEntity(companyHierarchyDto));
            CompanyEntity companyEntity = CompanyMapper.INSTANCE.companyDtoToCompanyEntity(companyDto);
            companyEntity.setDistribId(companyHierarchyEntity.getId());
            return CompanyMapper.INSTANCE.companyEntityToCompanyDto(companyRepository.createCompany(companyEntity));
        }
        return null;
    }

    public CompanyDto update(CompanyDto companyDto, Long id) {
        if (companyValidator.validateCompany(companyDto)) {
            CompanyEntity companyEntity = CompanyMapper.INSTANCE.companyDtoToCompanyEntity(companyDto);
            if (!companyDto.getDistribId().equals(id)) {
                CompanyHierarchyDto companyHierarchyDto = new CompanyHierarchyDto();
                companyHierarchyDto.setHasRight(companyDto.getHasRight());
                companyHierarchyDto.setHierdId(companyHierarchyRepository.getNodeText(id) +
                        (companyHierarchyRepository.getNumbersOfHeir(id) + 1) + "/");
                CompanyHierarchyEntity companyHierarchyEntity = companyHierarchyRepository.createHierarchy(CompanyHierarchyMapper.
                        INSTANCE.companyHrDtoToCompanyEntity(companyHierarchyDto));
                companyEntity.setDistribId(companyHierarchyEntity.getId());
            }

            System.out.println(CompanyMapper.INSTANCE.companyEntityToCompanyDto(companyEntity).toString());
            return CompanyMapper.INSTANCE.companyEntityToCompanyDto(companyRepository.createCompany(companyEntity));
        }
        return null;
    }

    public CompanyDto deleteById(Long id) {
        if (companyValidator.isIdCorrect(id)) {
            CompanyEntity companyEntity = companyRepository.findById(id);
            if (companyValidator.isCompanyExist(companyEntity)) {
                CompanyDto companyDto = CompanyMapper.INSTANCE.companyEntityToCompanyDto(companyRepository.deleteById(id));
                companyHierarchyRepository.deleteById(companyDto.getDistribId());
                return companyDto;
            }
        }
        return null;
    }

}
